import 'package:flutter/cupertino.dart';

class ChartModel {
  final String? name;
  final String? massage;
  final String? time;
  final String? profileUrl;

  ChartModel({this.name, this.massage, this.time, this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'You',
      massage: 'LATIHANN GAISSS !!!',
      time: '12.00',
      profileUrl: 'assets/img/nayla.jpg'),
  ChartModel(
      name: 'Zahra',
      massage: 'AYOO NGERJAIN TUGAS ',
      time: 'yesterday',
      profileUrl:
          'assets/img/zahra.jpg'),
  ChartModel(
      name: 'Syaqila',
      massage: 'AYOO IH MAINN',
      time: 'yesterday',
      profileUrl:
          'assets/img/syaqila.jpg'),
  ChartModel(
      name: 'Saskia',
      massage: 'ESKULL TROSS',
      time: 'yesterday',
      profileUrl:
          'assets/img/saskia.jpg'),
];
