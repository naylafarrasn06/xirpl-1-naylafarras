import 'package:flutter/material.dart';
import 'package:flutter_app/tugas/telegram/Telegram.dart';
import 'package:flutter_app/tugas/telegram/Home/homePage.dart';
import 'package:flutter_app/tugas/telegram/Login/loginPage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Login',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: Telegram(),
    );
  }
}
