import 'package:flutter/material.dart';
import 'package:berita/ulangan/Berita.dart';

class DetailBerita1 extends StatelessWidget {
  final String judul;
  final String deskripsi;
  final String imageUrl;

  DetailBerita1({required this.judul, required this.deskripsi, required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            "BERITA TERKINI",
            style: TextStyle(
              fontSize: 50,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              judul,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
            ),
            SizedBox(height: 10),

            Center(
              child: SizedBox(
                width: 500,
              child: Image.network(
                imageUrl,
              fit: BoxFit.cover,
              ),
              ),
            ),
              SizedBox(height: 20),

          Text(
              deskripsi,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            SizedBox(height: 20),

            Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: EdgeInsets.only(right: 40.0), // Sesuaikan jarak yang Anda inginkan di sini
                  child: ElevatedButton(
              onPressed: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => Berita(), // Gantilah dengan widget Berita Anda
              ),
            );
              },
                child: Text('Kembali'),
            ),
            ),
            ),
          ],
        ),
      ),
    ),
    );
  }
}

class DetailBerita2 extends StatelessWidget {
  final String judul;
  final String deskripsi;
  final String imageUrl;

  DetailBerita2({required this.judul, required this.deskripsi, required this.imageUrl});

  @override
 Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title: Center(
        child: Text(
          "BERITA TERKINI",
          style: TextStyle(
            fontSize: 50,
            fontWeight: FontWeight.normal,
          ),
        ),
      ),
    ),
    body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              judul,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
            ),
            SizedBox(height: 10),

            Center(
              child: SizedBox(
                width: 500,
                child: Image.network(
                  imageUrl,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(height: 20),

            Text(
              deskripsi,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            SizedBox(height: 20),

            Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: EdgeInsets.only(right: 40.0), // Sesuaikan jarak yang Anda inginkan di sini
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Berita(), // Gantilah dengan widget Berita Anda
                      ),
                    );
                  },
                  child: Text('Kembali'),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
}
class DetailBerita3 extends StatelessWidget {
  final String judul;
  final String deskripsi;
  final String imageUrl;

  DetailBerita3({required this.judul, required this.deskripsi, required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BERITA TERKINI"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              judul,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
            ),
            SizedBox(height: 10),
            Text(
              deskripsi,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            SizedBox(height: 20),
            Image.network(
              imageUrl,
              width: 500,
              fit: BoxFit.cover,
            ),
            SizedBox(height: 20),

            ElevatedButton(
  onPressed: () {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => Berita(), // Gantilah dengan widget Berita Anda
      ),
    );
  },
  child: Text('Kembali'),
),

          ],
        ),
      ),
    );
  }
}