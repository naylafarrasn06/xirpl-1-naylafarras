import 'package:flutter/material.dart';
import 'package:berita/ulangan/Models/Details.dart';

class Berita extends StatefulWidget {
  @override
  _BeritaState createState() => _BeritaState();
}

class _BeritaState extends State<Berita> {
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            "BERITA TERKINI",
            style: TextStyle(
              fontSize: 50,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          ),
        ],
      ),
      body: SingleChildScrollView(
  child: Column(
    children: <Widget>[
      SizedBox(height: 10.0), // Tambahkan jarak vertikal di antara AppBar dan kontainer pencarian
      Container(
        color: Color.fromARGB(169, 169, 169, 169), // Warna latar belakang di luar kotak pencarian
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: searchController,
              decoration: InputDecoration(
                hintText: "Search",
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.search), // Ikon pencarian di samping kotak pencarian
                filled: true, // Mengisi kotak dengan warna latar belakang
                  fillColor: Colors.white, // Warna latar belakang kotak pencarian (putih)
                
              ),
            ),
            // Widget lainnya di sini
          ],
        ),
        ),
         SizedBox(height: 10.0),
          Stack(
            children: [
              Container(
                width: 1340, // Atur lebar sesuai kebutuhan
                height: 250, // Atur tinggi sesuai kebutuhan
                child: Card(
                  color: Color.fromARGB(169, 169, 169, 169), // Ganti warna sesuai yang Anda inginkan
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          'Selasa, 26 Sep 2023 Detik.com',
                          style: TextStyle(
                            fontSize: 15, // Atur sesuai kebutuhan
                            fontWeight: FontWeight.normal, // Mengatur teks menjadi tidak bold
                          ),
                        ),
                      ),
                      ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0), // Tambahkan jarak di sini
                    subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start, // Aligment start agar teks mulai dari kiri
            children: [
              SizedBox(height: 10.0), // Tambahkan jarak di sini
            Text(
                'Kebakaran Gunung Bromo memberikan dampak kerugian negara yang cukup besar yakni mencapai Rp 89 miliar.',
            style: TextStyle(
              fontSize: 15, // Atur sesuai kebutuhan
              color: Colors.black, // Atur warna teks menjadi hitam
            ),
          ),
          SizedBox(height: 3.0), // Tambahkan jarak di sini
            Text(
              'Para pelaku pembakaran pun terancam sanksi denda atau penjara.Para pelaku pembakaran pun terancam sanksi denda atau penjara.',
            style: TextStyle(
              fontSize: 15, // Atur sesuai kebutuhan
              color: Colors.black, // Atur warna teks menjadi hitam
        ),
      ),
    ],
  ),
            title: Text(
                      'Kebakaran Bromo Bikin Negara Rugi Rp 89 M, Pelaku Terancam Denda Rp 1,5 M',
            style: TextStyle(
            fontSize: 25, // Atur sesuai kebutuhan
              fontWeight: FontWeight.bold,
    ),
  ),
)
          ],
        ),
      ),
    ),
              Positioned(
                bottom: 12, // Atur posisi tombol di bagian bawah
                right: 16, // Atur posisi tombol di bagian kanan
                child: ElevatedButton(
                  onPressed: () {
                    // Navigasi ke halaman detail berita saat tombol ditekan
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                         builder: (context) => DetailBerita1(
              judul: 'Selasa, 26 Sep 2023 Detik.com',
              imageUrl: 'https://akcdn.detik.net.id/community/media/visual/2023/09/23/menteri-lhk-memantau-kawasan-bromo-usai-kebakaran-2_169.jpeg?w=700&q=90',
              deskripsi: 'Mengutip detikTravel, Selasa (26/9/2024) Direktur Kajian Strategis Kemenparekraf/Barekraf Agustini Rahayu menyampaikan informasi terbaru soal kebakaran di Taman Nasional Bromo Tengger Semeru (TNBTS). Menurut data, Bromo mengalami kerugian besar akibat penutupan. \n\n'
                         '"Karena penutupan 13 hari, Bromo rugi Rp 89,7 miliar," ujar Ayu, sapaan Agustini Rahayu, dalam konferensi pers di Kantor Kemenparekraf, Jakarta, Senin (25/9) \n\n'
                         'Kemudian bagi para pelaku kebakaran Bromo terancam sanksi penjara atau denda sesuai Undang-undang Nomor 39 tentang Perlindungan dan Pengelolaan Lingkungan. \n\n'
                         '"Secara hukum ada denda potensial kehilangan berupa ancaman penjara 5 tahun atau denda paling banyak Rp 1,5 miliar," kata dia. \n\n'
                         'Sementara, calon pengantin yang melakukan foto prewedding dengan flare hingga terjadi kebakaran Bromo dikenakan hukuman wajib lapor. Mereka hanya berstatus saksi dalam kasus kebakaran di Bukit Teletubbies Gunung Bromo. Begitu pula dengan tiga kru Wedding Organizer (WO) prewedding itu. Berbeda nasib dengan kedua calon pengantin, manajer Wedding Organizer (WO) ditetapkan sebagai tersangka. \n\n'
                         'Prewedding itu dilakukan di Bukit Teletubbies, Gunung Bromo pada 6 September. Prewedding mereka menggunakan flare hingga memercikkan api di padang rumput. Akibat percikan api itulah mengakibatkan kebakaran yang cukup besar. \n\n'
                         'Berikut identitas enam wisatawan tersebut.\n' 
                         '1. Manajer WO, AW (41) asal Kabupaten Lumajang (tersangka)\n'
                         '2. Kru WO, MGG (38) asal Kelurahan Kedungdoro, Kecamatan Tegalsari Kota Surabaya (saksi) \n'
                         '3. Kru WO, ET (27) asal Kelurahan Klampis Ngasem, Kecamatan Sukolilo, Kota Surabaya (saksi) \n'
                         '4. Juru rias, ARVD (34) asal Kelurahan/Kecamatan Tandes, Kota Surabaya (saksi) \n'
                         '5. Calon pengantin pria HP (39) asal Kelurahan Kedungdoro, Kecamatan Tegalsari, Kota Surabaya (saksi) \n'
                         '6. Calon pengantin wanita PMP (26) asal Kelurahan Lrorok Pakjo, Kecamatan Ilir Barat 1, Kota Palembang (saksi) \n'
            ),
                      ),
                    );
                  },
                  child: Text('Selengkapnya'),
                ),
              ),
            ],
          ),

          SizedBox(height: 5.0),
           Stack(
            children: [
          Container(
            width: 1340, // Atur lebar sesuai kebutuhan
            height: 250, // Atur tinggi sesuai kebutuhan
          child: Card(
              color: Color.fromARGB(169, 169, 169, 169), // Ganti warna sesuai yang Anda inginkan
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0), 
              child: Text(
                '5 jam lalu Detik.com',
                style: TextStyle(
                  fontSize: 15, // Atur sesuai kebutuhan
                ),
              ),
            ),
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0), // Tambahkan jarak di sini
                    subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start, // Aligment start agar teks mulai dari kiri
            children: [
              SizedBox(height: 10.0), // Tambahkan jarak di sini
            Text(
                'Presiden Joko Widodo (Jokowi) memimpin upacara Hari Kesaktian Pancasila di Monumen Pancasila Sakti, Lubang Buaya, Jakarta Timur.',
            style: TextStyle(
              fontSize: 15, // Atur sesuai kebutuhan
              color: Colors.black, // Atur warna teks menjadi hitam
            ),
          ),
          SizedBox(height: 3.0), // Tambahkan jarak di sini
            Text(
              'Selesai pelaksanaan upacara, Jokowi meninjau serta berdoa di Sumur Lubang Buaya.',
            style: TextStyle(
              fontSize: 15, // Atur sesuai kebutuhan
              color: Colors.black, // Atur warna teks menjadi hitam
        ),
      ),
    ],
  ),
            title: Text(
                      'Hari Kesaktian Pancasila, Jokowi Berdoa di Sumur Lubang Buaya',
            style: TextStyle(
            fontSize: 25, // Atur sesuai kebutuhan
              fontWeight: FontWeight.bold,
    ),
  ),
)
          ],
        ),
      ),
    ),
    Positioned(
      bottom: 12, // Atur posisi tombol di bagian bawah
      right: 16, // Atur posisi tombol di bagian kanan
      child: ElevatedButton(
        onPressed: () {
          // Navigasi ke halaman detail berita saat tombol ditekan
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => DetailBerita2(
                judul: '5 jam lalu Detik.com',
                imageUrl: 'https://akcdn.detik.net.id/community/media/visual/2023/10/01/jokowi-berdoa-di-sumur-lubang-buaya_169.jpeg?w=700&q=90',
                deskripsi:'Pantauan detikcom di lokasi, Minggu (1/10/2023) momen tersebut terjadi usai pelaksanaan upacara. Turut hadir dalam peninjauan Wakil Presiden Ma\'ruf Amin. Selain itu, para Menteri Kabinet Indonesia Maju mengikuti Jokowi untuk meninjau Sumur Lubang Buaya. Peninjauan diiringi lagu "Gugur Bunga" karya Ismail Marzuki.\n\n'
                          'Sampai di depan Sumur Lubang Buaya, Jokowi tampak berdoa bersama sejumlah menteri Kabinet Indonesia Maju. Jokowi bersama sejumlah menteri lalu mengambil sikap berdoa, mengangkat kedua tangan.\n\n'
                          'Sebagai informasi, Sumur Lubang Buaya adalah tempat pembuangan perwira Angkatan Darat yang menjadi korban G30S PKI. Tubuh mereka dimasukkan ke dalam lubang kecil, sehingga lebih dari satu orang menumpuk di dalamnya.\n\n'
                          'Para korban yang sudah dievakuasi dari Lubang Buaya kemudian dimakamkan di Taman Makam Pahlawan (TMP) Kalibata, Jakarta Selatan.\n\n'
                          'Sebagaimana diketahui, setiap tanggal 1 Oktober, masyarakat Indonesia merayakan Hari Kesaktian Pancasila. Hari Kesaktian Pancasila ditetapkan oleh Presiden Soeharto setelah peristiwa Pengkhianatan G30S/PKI pada 1965.\n\n'
                          'Hari Kesaktian Pancasila diperingati untuk mengenang kembali bagaimana Pancasila sebagai dasar negara berdiri teguh dari berbagai upaya yang dilakukan untuk menjatuhkannya.\n\n'
                          'Keteguhan Pancasila yang diterjang berbagai upaya membelokkan ideologi bangsa menjadikannya pusaka yang diakui kesaktiannya. Peringatan ini juga sebagai tanda penghormatan pada para pejuang yang gugur dalam mempertahankan Pancasila dalam peristiwa Pengkhianatan G30S/PKI.\n\n'
                          'Pancasila dirumuskan oleh Presiden Sukarno pada saat Indonesia merdeka. Lahir dari objektivitas berdasarkan keadaan saat itu, Pancasila belum bisa dianggap pusaka yang sakti. Seiring berjalannya waktu kepemimpinan Sukarno, Pancasila diterima dengan tangan terbuka oleh masyarakat Indonesia sebagai dasar negara.'
              ),
            ),
          );
        },
        child: Text('Selengkapnya'),
      ),
    ),
  ],
),
          SizedBox(height: 5.0),
          Stack(
            children: [
              Container(
                width: 1340, // Atur lebar sesuai kebutuhan
                height: 250, // Atur tinggi sesuai kebutuhan
                child: Card(
                  color: Color.fromARGB(169, 169, 169, 169), // Ganti warna sesuai yang Anda inginkan
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          'Selasa, 22 Agu 2023 Detik Food.com',
                          style: TextStyle(
                            fontSize: 15, // Atur sesuai kebutuhan
                          ),
                        ),
                      ),
                     ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0), // Tambahkan jarak di sini
                    subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start, // Aligment start agar teks mulai dari kiri
            children: [
              SizedBox(height: 10.0), // Tambahkan jarak di sini
            Text(
                ' Pria ini sedang bertengkar dengan kekasih.Iapun minta tolong barista di Starbucks untuk menuliskan keluhannya, di gelas kopi yang ia belikan untuk kekasihnya.',
            style: TextStyle(
              fontSize: 15, // Atur sesuai kebutuhan
              color: Colors.black, // Atur warna teks menjadi hitam
            ),
          ),
          SizedBox(height: 3.0), // Tambahkan jarak di sini
    ],
  ),
            title: Text(
                      'Kocak! Ribut dengan Kekasih, Pria Ini Curhat Lewat Gelas Starbucks',
            style: TextStyle(
            fontSize: 25, // Atur sesuai kebutuhan
              fontWeight: FontWeight.bold,
    ),
  ),
)
                    ],
                  ),
                ),
              ),
              Positioned(
      bottom: 12, // Atur posisi tombol di bagian bawah
      right: 16, // Atur posisi tombol di bagian kanan
      child: ElevatedButton(
        onPressed: () {
          // Navigasi ke halaman detail berita saat tombol ditekan
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => DetailBerita2(
                judul: 'Selasa, 22 Agu 2023 Detik Food.com',
                imageUrl: 'https://akcdn.detik.net.id/community/media/visual/2023/08/21/kocak-ribut-dengan-kekasih-pria-ini-curhat-lewat-gelas-starbucks.png?w=700&q=90',
                deskripsi:'Banyak cara menunjukkan rasa kasih sayang, hingga meluapkan isi hati kepada pasangan. Salah satunya dengan mengirimkan makanan atau minuman ke pasangan. Cara ini juga biasanya dilakukan orang, jika sedang bertengkar dengan kekasih.\n\n'
                          'Seperti aksi seorang pembeli kopi di Starbucks Malaysia. Lewat akun TikTok barista Starbucks @taararichie (21/08), dirinya mendapatkan pesanan unik dan menarik dari seorang pembeli. \n\n'
                          'Lewat catatan di kolom pesanan, pembeli itu meminta agar barista menuliskan semua keluh kesahnya di gelas kopi yang akan dikirimkan ke alamat kekasihnya. Tak hanya satu atau dua kata saja, tapi kalimat curhatan panjang dituliskan oleh pembeli tersebut. Uniknya lagi, sang barista menyanggupi permintaan tersebut. Ia berusaha menuliskan semua isi hati pembeli tersebut ke atas gelas kopi.\n\n'
                          '"Kepada kakak (barista) cantik dan abang (barista) tampan, saya mau request buat catatan boleh gak? Tolong ditulis ya. Untuk Mya aka Hazirah. Hi, aku belikan minuman kesukaan kamu. Saya ingin tanya kenapa kamu berbuat seperti itu?" isi curhatan pembeli tersebut.\n\n'
                          'Di bawahnya, pembeli itu menuliskan bahwa apapun yang dilakukan pasangannya sudah tidak mempan untuknya. Karena dia sudah mati rasa, dan menganggap kata-kata manis pasangannya ini, sama seperti rasa kopi di Starbucks.\n\n'
                          '"Saya tahu kamu yang mengilang, rasanya jadi palsu. Asal blokir saja, takut kah? Sudah buat salah malah lari. Kenapa suruh pria itu yang balas, bukan kamu yang balas?" pungkas curhatan tersebut.\n\n'
                          'Menanggapi hal ini, netizen memberikan komentar yang beragam. Ada yang terhibur dengan curhatan ini, ada juga yang merasa bahwa pembeli ini merepotkan barista. \n\n'
                          '"Kalau saya jadi barista itu, saya tempel saja bon isi curhatannya jadi tidak repot dan capek," komen netizen. \n\n'
                          '"Aduh lucu sekali. Tapi gemas betul pria ini. Dia saking kesalnya sampai curhat panjang di gelas Starbucks, mana dikirimkan ke kekasihnya," sambung netizen. \n\n'
                          '"Buset, itu gelas Starbucks penuh dengan curhatan Abang. Kasian pacar abang, mau minum Starbucks malah harus baca curhatan abang di gelas," tulis netizen lainnya. \n\n'
                          'Banyak juga netizen yang tertawa karena usaha dari pria ini, untuk menghubungi pasangannya. \n\n'
                          '"Pasti semua nomor dan akun media sosial dia sudah diblokir. Sehingga dia tidak ada cara lain, selain mengirimkan pesan lewat gelas Starbucks," pungkas netizen. \n\n'
                          'Akhirnya kemasan Starbucks itu penuh dengan pesan-pesan manis dari pembeli untuk kekasihnya. \n\n'
              ),
            ),
          );
        },
        child: Text('Selengkapnya'),
      ),
    ),
  ],
),
          SizedBox(height: 5.0),
          
          // Anda dapat menambahkan lebih banyak Card sesuai kebutuhan
        ],
      ),
    ),
    );
  }
}